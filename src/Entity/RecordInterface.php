<?php

namespace Drupal\records\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides the interface for Record entities.
 *
 * A Record is a content entity related to the Records framework. Even though
 * the definition of a Record is as general as that of a Drupal entity, this
 * interface is provided to enable stricter  type control wherever that is
 * needed. For example, the account currency resolver is only meant to operate
 * on Record or User entities - otherwise its meaning is dubious. Resolver
 * implementations may therefore throw an `\InvalidArgumentException` when they
 * are given a different entity.
 *
 * Record entities commonly have the following characteristics:
 * - It belongs to an account (Account entities not implemented yet).
 * - It may have an owner (assignee).
 * - It may have a primary amount.
 * - It may have a normalized amount for multi-currency setups (not implemented
 *   yet).
 * - They may be configurable by an entity configurator plugin.
 *
 * Currently, there are no methods defined by the interface yet. The core
 * API will be methods related to the record's parent account, once accounts are
 * implemented.
 */
interface RecordInterface extends ContentEntityInterface {}
