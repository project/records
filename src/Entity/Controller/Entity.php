<?php

namespace Drupal\records\Entity\Controller;

use Drupal\Core\Entity\Controller\EntityController as EntityControllerBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;

/**
 * Provides a controller for entities with bundleable bundle entity types.
 *
 * The entity controller provided by core does not properly handle entity types
 * the bundle entity type of which is bundleable itself. We provide an
 * alternative `addPage` method here until the corresponding issue is resolved
 * and the MR is merged.
 *
 * See https://www.drupal.org/project/drupal/issues/3256948.
 */
class Entity extends EntityControllerBase {

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    $bundle_key = $entity_type->getKey('bundle');
    $bundle_entity_type_id = $entity_type->getBundleEntityType();
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];
    if ($bundle_entity_type_id) {
      $bundle_argument = $bundle_entity_type_id;
      $bundle_entity_type = $this->entityTypeManager->getDefinition($bundle_entity_type_id);
      $build['#cache']['tags'] = $bundle_entity_type->getListCacheTags();

      // Filter out the bundles the user doesn't have access to.
      $access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);
      foreach ($bundles as $bundle_name => $bundle_info) {
        $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
        if (!$access->isAllowed()) {
          unset($bundles[$bundle_name]);
        }
        $this->renderer->addCacheableDependency($build, $access);
      }
      // Add descriptions from the bundle entities.
      $bundles = $this->loadBundleDescriptions($bundles, $bundle_entity_type);
    }
    else {
      $bundle_argument = $bundle_key;
    }

    $form_route_name = 'entity.' . $entity_type_id . '.add_form';
    // Redirect if there's only one bundle available.
    $bundle_count = count($bundles);
    if ($bundle_count === 1) {
      $bundle_names = array_keys($bundles);
      $bundle_name = reset($bundle_names);
      return $this->redirect($form_route_name, [$bundle_argument => $bundle_name]);
    }
    // Show a message shown when there are no bundles.
    elseif ($bundle_entity_type_id && $bundle_count === 0) {
      $build['#add_bundle_message'] = $this->buildAddBundleMessage($bundle_entity_type);
      return $build;
    }

    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => $bundle_info['description'] ?? '',
        'add_link' => Link::createFromRoute($bundle_info['label'], $form_route_name, [$bundle_argument => $bundle_name]),
      ];
    }

    return $build;
  }

  /**
   * Builds the message displayed when there is no bundle available.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $bundle_entity_type
   *   The entity type providing the bundles for the entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated message.
   */
  protected function buildAddBundleMessage(EntityTypeInterface $bundle_entity_type) {
    $bundle_entity_type_id = $bundle_entity_type->id();
    $bundle_entity_type_label = $bundle_entity_type->getSingularLabel();

    $link_route_name = NULL;
    if ($bundle_entity_type->hasLinkTemplate('add-page')) {
      $link_route_name = "entity.{$bundle_entity_type_id}.add_page";
    }
    else {
      $link_route_name = "entity.{$bundle_entity_type_id}.add_form";
    }

    $link_text = $this->t('Add a new @entity_type.', ['@entity_type' => $bundle_entity_type_label]);
    return $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $bundle_entity_type_label,
      '@add_link' => Link::createFromRoute($link_text, $link_route_name)->toString(),
    ]);
  }

}
