<?php

namespace Drupal\records\Entity\Controller;

use Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Base class for configurable bundle config entities provided by Records.
 */
class BundleConfigEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * The entity configurator plugin manager.
   *
   * @var \Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new BundleConfigEntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface $plugin_manager
   *   The income type configurator plugin manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    PluginManagerInterface $plugin_manager
  ) {
    parent::__construct($entity_type, $storage);

    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('ID');
    $header['description'] = $this->t('Description');
    $header['configurator_label'] = $this->t('Configurator');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['description'] = $entity->getDescription();

    $plugin = $this->pluginManager->createInstanceForEntity($entity);
    $row['configurator_label'] = $plugin->label();

    return $row + parent::buildRow($entity);
  }

}
