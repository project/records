<?php

namespace Drupal\records\Entity\Form;

use Drupal\records\Entity\StatefulEntityInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\user\EntityOwnerInterface;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base controller for content entity forms.
 *
 * It facilitates building forms for content entities. It should be used for
 * add/edit forms.
 *
 * It does the following:
 * - Handles owner, changed and state fields.
 * - Provides standard redirects when the forms are i.e. to the entity view page
 *   if the user has permissions for it, to the default entity list page
 *   otherwise.
 */
abstract class ContentEntityBase extends ContentEntityForm {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a ContentEntityBase object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct(
      $entity_repository,
      $entity_type_bundle_info,
      $time
    );

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['advanced'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 99,
    ];

    $form = parent::form($form, $form_state);
    $entity = $this->getEntity();

    // State and ownership information - for display purposes only.
    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => TRUE,
    ];

    if ($entity instanceof StatefulEntityInterface) {
      $form['meta']['state'] = [
        '#type' => 'item',
        '#markup' => $this->getStateLabel(),
        '#wrapper_attributes' => ['class' => ['entity-meta__title']],
      ];
    }
    if ($entity instanceof EntityChangedInterface) {
      $form['meta']['changed'] = [
        '#type' => 'item',
        '#title' => $this->t('Last saved'),
        '#markup' => $this->getChangedLabel(),
        '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
      ];
    }

    if ($entity instanceof EntityOwnerInterface) {
      $form['meta']['owner'] = [
        '#type' => 'item',
        '#title' => $this->t('Owner'),
        '#markup' => $this->entity->getOwner()->getAccountName(),
        '#wrapper_attributes' => ['class' => ['entity-meta__author']],
      ];

      // Ownership.
      $form['owner'] = [
        '#type' => 'details',
        '#title' => $this->t('Ownership information'),
        '#access' => !$this->entity->isNew(),
        '#group' => 'advanced',
      ];
      $form['owner_id']['#group'] = 'owner';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $return = parent::save($form, $form_state);

    $entity = $this->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    if ($entity->access('view')) {
      $form_state->setRedirect(
        "entity.{$entity_type_id}.canonical",
        [$entity_type_id => $entity->id()]
      );
    }
    else {
      $form_state->setRedirect("entity.{$entity_type_id}.collection");
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBundleEntity() {
    if ($this->entity instanceof HasBundleEntityInterface) {
      return parent::getBundleEntity();
    }

    return $this->entityTypeManager
      ->getStorage($this->entity->getEntityType()->getBundleEntityType())
      ->loadWithPluginInstantiated($this->entity->bundle());
  }

  /**
   * Returns the label of the record's current state.
   *
   * @return string
   *   The current state of the record, or the initial state if we are creating
   *   a new record.
   */
  protected function getStateLabel() {
    $state = $this->entity->getState();

    if (!$this->entity->isNew()) {
      return $state->getLabel();
    }

    $states = $state->getWorkflow()->getStates();
    return reset($states)->getLabel();
  }

  /**
   * Returns the label of the record's last changed time.
   *
   * @return string
   *   The formatted last changed time, or a string indicating that the record
   *   has not been saved yet if we are creating a new record.
   */
  protected function getChangedLabel() {
    if ($this->entity->isNew()) {
      return $this->t('Not saved yet');
    }

    return $this->dateFormatter->format(
      $this->entity->getChangedTime(),
      'short'
    );
  }

}
