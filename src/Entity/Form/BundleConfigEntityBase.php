<?php

namespace Drupal\records\Entity\Form;

use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\records\EntityConfigurator\Entity\Form\ConfigFormTrait;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

use Psr\Log\LoggerInterface;

/**
 * Base controller for configuration entity forms.
 *
 * It facilitates building forms for configuration entities the provide bundles
 * for Record entities. It should be used when the configuration entity:
 * - Has a label and description.
 * - Is configurable by an entity configurator plugin.
 *
 * It does the following:
 * - Builds the base form elements i.e. ID, label, description.
 * - Builds a form element indicating the type of the configurator plugin.
 * - Builds the configurator plugin form.
 */
class BundleConfigEntityBase extends EntityForm {

  use ConfigFormTrait;

  /**
   * Constructs a new BundleConfigEntityBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger for the module providing the form's entity.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The entity configurator plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    PluginManagerInterface $plugin_manager,
    TranslationInterface $string_translation
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->pluginManager = $plugin_manager;

    // Injections required by traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->getEntity();
    $entity_type = $this->entityTypeManager->getDefinition(
      $entity->getEntityTypeId()
    );
    $target_entity_type = $this->entityTypeManager->getDefinition(
      $entity_type->getBundleOf()
    );

    // Entity form elements.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$entity->isNew(),
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t(
        'A brief description of the @entity_type_label. It will be
         displayed on the <em>Add @target_entity_type_label</em> page.',
        [
          '@entity_type_label' => strtolower($entity_type->getLabel()),
          '@target_entity_type_label' => strtolower($target_entity_type->getLabel()),
        ]
      ),
      '#default_value' => $entity->getDescription(),
    ];

    // Configurator plugin form elements.
    if ($entity instanceof ConfigurableEntityInterface) {
      $form = $this->buildEntityConfiguratorForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->getEntity() instanceof ConfigurableEntityInterface) {
      $this->validateEntityConfiguratorForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    if ($this->getEntity() instanceof ConfigurableEntityInterface) {
      $this->submitEntityConfiguratorForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->set('type', trim($entity->id()));
    $entity->set('label', trim($entity->label()));
    $entity_type = $this->entityTypeManager->getDefinition(
      $entity->getEntityTypeId()
    );

    $status = $entity->save();
    $message_context = [
      '@entity_type_label' => $entity_type->getLabel(),
      '%label' => $entity->label(),
    ];
    $logger_context = [
      'link' => $entity->toLink($this->t('Edit'), 'edit-form')->toString(),
    ] + $message_context;

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t(
        '@entity_type_label %label has been updated.',
        $message_context
      ));
      $this->logger->notice(
        '@entity_type_label %label has been updated.',
        $logger_context
      );
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t(
        '@entity_type_label %label has been added.',
        $message_context
      ));
      $this->logger->notice(
        '@entity_type_label %label has been added.',
        $logger_context
      );
    }

    $form_state->setRedirect('entity.' . $entity->getEntityTypeId() . '.collection');
  }

  /**
   * Determines if an entity already exists for the given ID.
   *
   * @param string $id
   *   The entity ID.
   *
   * @return bool
   *   TRUE if the entity exists, FALSE otherwise.
   */
  public function exists($id) {
    $entity = $this->entityTypeManager
      ->getStorage($this->getEntity()->getEntityTypeId())
      ->load($id);
    return $entity ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    if ($this->getEntity() instanceof ConfigurableEntityInterface) {
      return $this->buildEntityConfiguratorActions(
        parent::actions($form, $form_state)
      );
    }

    return parent::actions($form, $form_state);
  }

}
