<?php

namespace Drupal\records\Entity;

use Drupal\records\MachineName\Field\Record as RecordField;

/**
 * Trait that facilitates implementing content entities that have state.
 *
 * @see \Drupal\records\Entity\StatefulEntityInterface
 */
trait StatefulContentEntityTrait {

  /**
   * Returns the entity state.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The entity state.
   */
  public function getState() {
    return $this->get(RecordField::STATE)->first();
  }

}
