<?php

namespace Drupal\records\Entity;

use Drupal\records\EntityConfigurator\Entity\ConfigEntityTrait;
use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Base class for bundle configuration entities provided by Records.
 */
abstract class BundleConfigEntityBase extends ConfigEntityBundleBase implements
  ConfigurableEntityInterface,
  EntityDescriptionInterface {

  use ConfigEntityTrait;

  /**
   * The machine name of the entity.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the entity.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the entity.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
