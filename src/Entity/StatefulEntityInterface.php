<?php

namespace Drupal\records\Entity;

/**
 * Interface for entities provided by Records that have state.
 */
interface StatefulEntityInterface {

  /**
   * Returns the entity state.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The entity state.
   */
  public function getState();

}
