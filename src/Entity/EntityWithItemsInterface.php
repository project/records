<?php

namespace Drupal\records\Entity;

/**
 * Interface for entities that have primary children items.
 *
 * This interface relates to the primary items of an entity e.g. income items,
 * expense items etc. The purpose and function of the items may be different for
 * each parent entity. Any items of a secondary nature that a parent entity may
 * have are outside of the scope of this interface and they should not be
 * set/gotten using the methods provided here.
 */
interface EntityWithItemsInterface {

  /**
   * Returns the entity items.
   *
   * @return \Drupal\records\Entity\EntityItemInterface[]
   *   The items.
   */
  public function getItems();

  /**
   * Returns the IDs of all entity item.
   *
   * @return string[]
   *   An array containing the IDs of all items.
   */
  public function getItemIds();

  /**
   * Sets the entity items.
   *
   * @param \Drupal\records\Entity\EntityItemInterface[] $items
   *   The items.
   *
   * @return $this
   */
  public function setItems(array $items);

}
