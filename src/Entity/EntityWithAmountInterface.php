<?php

namespace Drupal\records\Entity;

use Drupal\commerce_price\Price;

/**
 * Interface for entities that have a primary amount.
 */
interface EntityWithAmountInterface {

  /**
   * Returns the amount of the record.
   *
   * @return \Drupal\commerce_price\Price
   *   The amount.
   */
  public function getAmount();

  /**
   * Sets the amount of the record.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   */
  public function setAmount(Price $amount);

}
