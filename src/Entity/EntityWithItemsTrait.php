<?php

namespace Drupal\records\Entity;

use Drupal\records\MachineName\Field\Record as RecordField;

/**
 * Trait that facilitates implementing entities that have primary items.
 *
 * @see \Drupal\records\Entity\EntityWithItemsInterface
 */
trait EntityWithItemsTrait {

  /**
   * Returns the entity items.
   *
   * @return \Drupal\records\Entity\EntityItemInterface[]
   *   The items.
   */
  public function getItems() {
    return $this->get(RecordField::ITEMS)->referencedEntities();
  }

  /**
   * Returns the IDs of all entity item.
   *
   * @return string[]
   *   An array containing the IDs of all items.
   */
  public function getItemIds() {
    return array_column(
      $this->get(RecordField::ITEMS)->getValue(),
      'target_id'
    );
  }

  /**
   * Sets the entity items.
   *
   * @param \Drupal\records\Entity\EntityItemInterface[] $items
   *   The items.
   *
   * @return $this
   */
  public function setItems(array $items) {
    $this->set(RecordField::ITEMS, $items);
    return $this;
  }

}
