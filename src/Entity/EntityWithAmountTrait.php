<?php

namespace Drupal\records\Entity;

use Drupal\commerce_price\Price;
use Drupal\records\MachineName\Field\Record as RecordField;

/**
 * Trait that facilitates implementing records that have a primary amount.
 *
 * @see \Drupal\records\Entity\EntityWithAmountInterface
 */
trait EntityWithAmountTrait {

  /**
   * Returns the amount of the record.
   *
   * @return \Drupal\commerce_price\Price
   *   The amount.
   */
  public function getAmount() {
    return $this->get(RecordField::AMOUNT)->first()->toPrice();
  }

  /**
   * Sets the amount of the record.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   */
  public function setAmount(Price $amount) {
    $this->set(RecordField::AMOUNT, $amount);
  }

}
