<?php

namespace Drupal\records\Entity;

/**
 * The interface for entities that are primary items of a parent entity.
 *
 * The purpose and function of the items may be different for each parent
 * entity. For example income items, expense items etc.
 */
interface EntityItemInterface {}
