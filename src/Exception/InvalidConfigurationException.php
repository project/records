<?php

namespace Drupal\records\Exception;

/**
 * Exception when invalid or insufficient configuration has been provided.
 */
class InvalidConfigurationException extends \Exception {
}
