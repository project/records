<?php

namespace Drupal\records\EntityConfigurator\Entity;

/**
 * The interface for entities that have a configurable bundle entity.
 */
interface HasBundleEntityInterface {}
