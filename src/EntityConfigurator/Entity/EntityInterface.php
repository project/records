<?php

namespace Drupal\records\EntityConfigurator\Entity;

use Drupal\records\EntityConfigurator\Plugin\PluginInterface;

/**
 * The interface for entities that are configurable by a configurator plugin.
 */
interface EntityInterface {

  /**
   * Returns the ID of the entity configurator plugin.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId();

  /**
   * Sets the ID of the entity configurator plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return $this
   */
  public function setPluginId(string $plugin_id);

  /**
   * Returns the entity configurator plugin.
   *
   * @return \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   *   The plugin.
   */
  public function getPlugin();

  /**
   * Sets the entity configurator plugin.
   *
   * It also updates the plugin configuration stored in the entity.
   *
   * @param \Drupal\records\EntityConfigurator\Plugin\PluginInterface $plugin
   *   The plugin.
   */
  public function setPlugin(PluginInterface $plugin);

  /**
   * Returns the entity configurator plugin configuration.
   *
   * phpcs:disable
   * @return array
   *   The plugin configuration.
   *
   * @I Add method for getting a plugin configuration property
   * phpcs:enable
   */
  public function getPluginConfiguration();

}
