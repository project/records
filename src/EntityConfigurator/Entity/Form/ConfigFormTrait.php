<?php

namespace Drupal\records\EntityConfigurator\Entity\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Trait that facilitates implementing configurable config entity forms.
 */
trait ConfigFormTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity configurator plugin manager.
   *
   * @var \Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Submit callback for creating fields for the entity.
   *
   * The fields are defined by the entity configurator plugin.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function createFields(array $form, FormStateInterface $form_state) {
    $field_definitions = $this->getPlugin()->buildFieldDefinitions();
    $entity = $this->getEntity();
    $target_entity_type_id = $this->entityTypeManager
      ->getDefinition($entity->getEntityTypeId())
      ->getBundleOf();

    foreach ($field_definitions as $field_name => $field_definition) {
      $field_definition->setTargetEntityTypeId($target_entity_type_id);
      $field_definition->setTargetBundle($entity->id());
      $field_definition->setName($field_name);

      $this->createField(
        $field_name,
        $target_entity_type_id,
        $field_definition,
        $entity
      );
    }
  }

  /**
   * Builds form elements related to the entity configurator plugin.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildEntityConfiguratorForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $plugin = $this->getPlugin();

    // This is just informational for the user. If the entity is new the plugin
    // ID is taken from the URL.
    // See `\Drupal\Core\Entity\EntityForm::getEntityFromRouteMatch()`.
    $form['configurator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Configurator'),
      '#options' => [
        $plugin->getPluginId() => $plugin->label(),
      ],
      '#default_value' => $plugin->getPluginId(),
      '#required' => TRUE,
    ];
    // Add the plugin configuration form elements.
    $form = $plugin->buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * Builds the form actions as required by the entity configurator plugin.
   *
   * We add the callback to create fields defined by the entity configurator.
   *
   * @param array $actions
   *   The form actions render array.
   *
   * @return array
   *   The updated form actions render array.
   */
  protected function buildEntityConfiguratorActions(array $actions) {
    $actions['submit']['#submit'] = array_merge(
      $actions['submit']['#submit'],
      ['::createFields']
    );

    return $actions;
  }

  /**
   * Runs any validations defined by the entity configurator plugin.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function validateEntityConfiguratorForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->getPlugin()->validateConfigurationForm($form, $form_state);
  }

  /**
   * Runs any submit actions defined by the entity configurator plugin.
   *
   * It also updates the entity so that the plugin ID and configuration is saved
   * with it.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function submitEntityConfiguratorForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $plugin = $this->getPlugin();
    $plugin->submitConfigurationForm($form, $form_state);
    $this->getEntity()->setPlugin($plugin);
  }

  /**
   * Creates the field for the given name and definition.
   *
   * The field storage and the field instance will be created only if they don't
   * exist. No errors are raised if they do.
   *
   * phpcs:disable
   * @param string $field_name
   *   The field machine name.
   * @param string $target_entity_type_id
   *   The ID of the target entity type i.e. the entity of which this form's
   *   entity is bundle of.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity to add the field to.
   *
   * @I Detect collissions between fields provided by different plugins
   *    type     : bug
   *    priority : normal
   *    labels   : entity-configurator
   *    notes    : Maybe that should happen both here and when loading plugin
   *               definitions i.e. in the plugin manager so that errors can be
   *               displayed during cache rebuild before reaching here.
   * phpcs:enable
   */
  protected function createField(
    string $field_name,
    string $target_entity_type_id,
    FieldDefinitionInterface $field_definition,
    ConfigEntityInterface $entity
  ) {
    $field_storage_config_storage = $this->entityTypeManager
      ->getStorage('field_storage_config');
    $field_config_storage = $this->entityTypeManager
      ->getStorage('field_config');

    // Create the field storage configuration, if it does not exist.
    $field_storage = $field_storage_config_storage->load(
      "{$target_entity_type_id}.{$field_name}"
    );
    if (!$field_storage) {
      $field_storage = $field_storage_config_storage->create([
        'field_name' => $field_name,
        'entity_type' => $target_entity_type_id,
        'type' => $field_definition->getType(),
        'cardinality' => $field_definition->getCardinality(),
        'settings' => $field_definition->getSettings(),
        'translatable' => $field_definition->isTranslatable(),
      ]);
      $field_storage_config_storage->save($field_storage);
    }

    // Create the field configuration, if it does not exist.
    $bundle = $entity->id();
    $field_config = $field_config_storage->load(
      "{$target_entity_type_id}.{$bundle}.{$field_name}"
    );
    if (!$field_config) {
      $field_config = $field_config_storage->create([
        'field_storage' => $field_storage,
        'bundle' => $bundle,
        'label' => $field_definition->getLabel(),
        'required' => $field_definition->isRequired(),
        'settings' => $field_definition->getSettings(),
        'translatable' => $field_definition->isTranslatable(),
        'default_value' => $field_definition->getDefaultValueLiteral(),
        'default_value_callback' => $field_definition->getDefaultValueCallback(),
      ]);
      $field_config_storage->save($field_config);
    }
  }

  /**
   * Returns the configurator plugin for the entity.
   *
   * @return \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   *   The entity configurator plugin.
   */
  protected function getPlugin() {
    $entity = $this->getEntity();
    $plugin = $entity->getPlugin();
    if ($plugin) {
      return $plugin;
    }

    $plugin = $this->pluginManager->createInstance(
      $entity->getPluginId(),
      $entity->getPluginConfiguration() ?? []
    );
    $entity->setPlugin($plugin);

    return $plugin;
  }

}
