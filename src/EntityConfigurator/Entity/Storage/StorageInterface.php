<?php

namespace Drupal\records\EntityConfigurator\Entity\Storage;

/**
 * The interface for storages of configurable entities.
 */
interface StorageInterface {

  /**
   * Returns the entity with the given ID.
   *
   * When loading an entity that is configurable via a configurator plugin using
   * the `load` method, the plugin is not instantiated and the plugin
   * configuration stored in the entity is not complete. This is because it only
   * contains the plugin configuration loaded from the configuration storage.
   *
   * When the plugin is actually instantiated, the plugin configuration is
   * merged with any configuration added by the plugin class, such as by the
   * `defaultConfiguration` method.
   *
   * We do not want to instantiate the plugin within the entity class because
   * because that would require getting other services (the plugin manager)
   * within the entity class via the Drupal object and that is not really a good
   * practice. It also exceeds the scope of the entity class.
   *
   * We therefore provide a method that facilitates instantiating the plugin
   * when loading an entity.
   *
   * @param string $id
   *   The ID of the entity to load.
   *
   * @return \Drupal\records\EntityConfigurator\Entity\EntityInterface|null
   *   The entity, or NULL if no matching entity is found.
   */
  public function loadWithPluginInstantiated(string $id);

}
