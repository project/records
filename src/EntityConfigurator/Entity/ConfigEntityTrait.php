<?php

namespace Drupal\records\EntityConfigurator\Entity;

use Drupal\records\EntityConfigurator\Plugin\PluginInterface;

/**
 * Trait that facilitates implementing plugin configurable config entities.
 *
 * While the interface would be the same, the implementation is different than a
 * potential content entity trait for two reasons:
 * - Property naming conventions are different.
 * - Property getting/setting is different.
 *
 * @see \Drupal\records\EntityConfigurator\Entity\EntityInterface
 */
trait ConfigEntityTrait {

  /**
   * The entity configurator plugin ID.
   *
   * @var string
   * phpcs:disable
   */
  protected $plugin_id;

  /**
   * phpcs:enable
   * The entity configurator plugin configuration.
   *
   * @var array
   * phpcs:disable
   */
  protected $plugin_config = [];

  /**
   * phpcs:enable
   * The entity configurator plugin.
   *
   * @var \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   */
  protected $plugin;

  /**
   * Returns the ID of the entity configurator plugin.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId() {
    return $this->plugin_id;
  }

  /**
   * Sets the ID of the entity configurator plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return $this
   */
  public function setPluginId(string $plugin_id) {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * Returns the entity configurator plugin.
   *
   * @return \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   *   The plugin.
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * Sets the entity configurator plugin.
   *
   * It also updates the plugin configuration stored in the entity.
   *
   * @param \Drupal\records\EntityConfigurator\Plugin\PluginInterface $plugin
   *   The plugin.
   */
  public function setPlugin(PluginInterface $plugin) {
    $this->plugin = $plugin;
    $this->plugin_config = $plugin->getConfiguration();
    return $this;
  }

  /**
   * Returns the entity configurator plugin configuration.
   *
   * @return array
   *   The plugin configuration.
   */
  public function getPluginConfiguration() {
    return $this->plugin_config;
  }

}
