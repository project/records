<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin\PluginTrait;

use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\Form\FormStateInterface;

/**
 * Facilitates implementing records that have a pre-configured title.
 *
 * Configuration options:
 * - title: (Optional) If set, the title of all records of types that are
 *     configured by plugins using this trait will be set to this value. If not
 *     set, no title will be programmatically generated. In that case, the
 *     record form display should be configured to expose the title
 *     field. Otherwise the income item will end up without a title.
 */
trait FixedTitleTrait {

  /**
   * Returns the default configuration for plugins that provide a title.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultFixedTitleConfiguration() {
    return ['title' => NULL];
  }

  /**
   * Returns the form with added elements related to title.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  public function buildFixedTitleConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t(
        'The title that will be given to records of types that use this
         configurator plugin. If not set, the title field should be exposed on
         the record form display otherwise such records may end up not having a
         title.'
      ),
      '#default_value' => $this->configuration['title'],
    ];
    return $form;
  }

  /**
   * Runs the form submission actions for plugins that provide a title.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitFixedTitleConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();
    $this->configuration['title'] = $values['title'] ?: NULL;
  }

  /**
   * Retrieves and sets the entity title from the plugin configuration.
   *
   * @param \Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface $entity
   *   The entity.
   */
  protected function setTitleFromConfiguration(
    HasBundleEntityInterface $entity
  ) {
    // We do not unset a potentially existing title if we don't have a
    // pre-configured title. Applications may have another mechanism to set the
    // title, let's not break them. They should not be using this trait in such
    // cases, but they may have whatever unexpected reasons for doing so.
    // Moreover, records are always meant to have a title and we should never
    // unset a title unless when replacing it with another one.
    if ($this->configuration['title'] !== NULL) {
      $entity->set(RecordField::TITLE, $this->configuration['title']);
    }
  }

}
