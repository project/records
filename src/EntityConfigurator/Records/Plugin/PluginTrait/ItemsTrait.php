<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin\PluginTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\records\Entity\EntityWithItemsInterface;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Facilitates implementing entities that reference one or more items.
 */
trait ItemsTrait {

  /**
   * Returns the field definitions required for referencing items.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildItemsFieldDefinitions() {
    $fields = [];

    $fields[RecordField::ITEMS] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Items'))
      ->setDescription(new TranslatableMarkup('The items for the entity.'))
      ->setCardinality(BundleFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Generates and sets the amount from the entity items.
   *
   * The amount generated is the sum of the amounts of all items.
   *
   * @param \Drupal\records\Entity\EntityWithItemsInterface $entity
   *   The entity.
   */
  public function setAmountFromItems(EntityWithItemsInterface $entity) {
    $amount = NULL;
    foreach ($entity->getItems() as $item) {
      $item_amount = $item->get(RecordField::AMOUNT)->first()->toPrice();
      $amount = $amount ? $amount->add($item_amount) : $item_amount;
    }

    $entity->set(RecordField::AMOUNT, $amount);
  }

}
