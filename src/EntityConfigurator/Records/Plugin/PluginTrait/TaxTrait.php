<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin\PluginTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Facilitates implementing entities that are taxes or are taxable/non-taxable.
 */
trait TaxTrait {

  /**
   * Returns the field definitions required for entities that represent tax.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildTaxFieldDefinitions() {
    $fields = [];

    $fields[RecordField::IS_TAX] = BundleFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Is tax?'))
      ->setDescription(new TranslatableMarkup(
        'Indicates whether the entity represents tax.'
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Returns the field definitions required for entities that are taxable.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildTaxableFieldDefinitions() {
    $fields = [];

    $fields[RecordField::IS_TAXABLE] = BundleFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Is taxable?'))
      ->setDescription(new TranslatableMarkup(
        'Indicates whether the entity is taxable.'
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Returns the field definitions required for entities that are taxed with VAT.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildVatFieldDefinitions() {
    $fields = [];

    $fields[RecordField::IS_VAT_EXEMPT] = BundleFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Is VAT-exempt?'))
      ->setDescription(new TranslatableMarkup(
        'Indicates whether the entity is exempt from VAT collection.'
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
