<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin\PluginTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Facilitates implementing entities that have a reference number.
 */
trait ReferenceTrait {

  /**
   * Returns the field definitions required for having a reference number.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildReferenceFieldDefinitions() {
    $fields = [];

    $fields[RecordField::REFERENCE] = BundleFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Reference number'))
      ->setDescription(new TranslatableMarkup(
        'The reference number for the entity.'
      ))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Generates and sets the entity title from the reference number field.
   *
   * @param \Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface $entity
   *   The entity.
   */
  protected function setTitleFromReference(
    HasBundleEntityInterface $entity
  ) {
    $entity->set(
      RecordField::TITLE,
      $entity->get(RecordField::REFERENCE)->value
    );
  }

}
