<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase as CorePluginBase;

/**
 * Base class for entity configurator plugins provided by Records.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface {

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Always include the default configuration.
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => [$this->pluginDefinition['provider']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Override this function if you need to do something specific to the
   * submitted data before it is saved as configuration on the plugin.
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   *
   * As with all Drupal entities, field machine names must always be unique
   * across all entity bundles. Currently, there is no programmatic detection
   * of conflicts between fields provided by different plugins.
   *
   * It is therefore recommended to leave unprefixed field machine names for the
   * exclusive use of official Records modules. Use an appropriate prefix for
   * fields provided by other contrib or custom modules, such as the name of the
   * module or the plugin ID.
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
