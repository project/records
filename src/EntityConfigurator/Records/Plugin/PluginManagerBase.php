<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin;

use Drupal\records\EntityConfigurator\Plugin\PluginManagerTrait as ConfiguratorPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Base manager of entity configurator plugins provided by Records.
 */
class PluginManagerBase extends DefaultPluginManager implements
  PluginManagerInterface {

  use ConfiguratorPluginManagerTrait;

  /**
   * {@inheritdoc}
   */
  public function getAllLabels() {
    $plugins = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
      $plugins[$plugin_id] = $plugin_definition['label'];
    }
    asort($plugins);

    return $plugins;
  }

}
