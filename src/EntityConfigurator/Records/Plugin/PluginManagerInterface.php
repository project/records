<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin;

use Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface as ConfiguratorPluginManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Interface for managers of entity configurator plugins provided by Records.
 */
interface PluginManagerInterface extends
  ComponentPluginManagerInterface,
  ConfiguratorPluginManagerInterface {

  /**
   * Returns all plugin labels.
   *
   * @return array
   *   Returns an array of all plugin labels keyed by plugin ID.
   */
  public function getAllLabels();

}
