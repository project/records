<?php

namespace Drupal\records\EntityConfigurator\Records\Plugin;

use Drupal\records\EntityConfigurator\Plugin\PluginInterface as ConfiguratorPluginInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides the interface for entity configurator plugins provided by Records.
 */
interface PluginInterface extends
  ConfigurableInterface,
  DependentPluginInterface,
  ConfiguratorPluginInterface,
  PluginFormInterface,
  PluginInspectionInterface {

  /**
   * Returns the plugin label.
   *
   * @return string
   *   The income item configurator label.
   */
  public function label();

}
