<?php

namespace Drupal\records\EntityConfigurator\Plugin;

use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;

/**
 * Defines the interface for plugins that act on entity pre-save.
 *
 * Plugin that implement this interface are given the opportunity to alter the
 * entity when it is being saved. The most common use case is to programmaticaly
 * calculate values for fields such as the amount of an entity based on its
 * items, or the title based on a reference number.
 *
 * Such programmatically generated values are still defined as fields for the
 * following reasons:
 * - To make it easy to expose them in views.
 * - For caching purposes i.e. so that the amount does not have to be calculated
 *   every time the entity is rendered.
 * - To facilitate cases where some bundles may require the value to be
 *   programmatically generated, while other bundles may require manual entry.
 *
 * @see \Drupal\records\Hook\EntitySave
 */
interface EntityPreSaveInterface {

  /**
   * Processes the given entity when being saved.
   *
   * Called by `hook_entity_presave()`.
   *
   * @param \Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface $entity
   *   The entity.
   */
  public function entityPreSave(HasBundleEntityInterface $entity);

}
