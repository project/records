<?php

namespace Drupal\records\EntityConfigurator\Plugin;

use Drupal\records\EntityConfigurator\Entity\EntityInterface;

/**
 * The interface for entity configurator plugin managers.
 */
interface PluginManagerInterface {

  /**
   * Creates an instance based on the plugin info of the given entity.
   *
   * @param \Drupal\records\EntityConfigurator\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   *   The plugin.
   */
  public function createInstanceForEntity(
    EntityInterface $entity
  );

}
