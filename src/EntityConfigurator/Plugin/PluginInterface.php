<?php

namespace Drupal\records\EntityConfigurator\Plugin;

use Drupal\entity\BundlePlugin\BundlePluginInterface;

/**
 * The interface for config entity configurator plugins.
 *
 * phpcs:disable
 * @I Differentiate between config and content entities
 *    type     : feature
 *    priority : low
 *    labels   : entity-configurator
 *    notes    : Having the plugin to inherit the `BundlePluginInterface` -
 *               which is required so that we can declared it to be a bundle
 *               plugin the plugin on configuration entities - is the main issue
 *               preventing us to use configurator plugins on content entities.
 * phpcs:enable
 */
interface PluginInterface extends BundlePluginInterface {}
