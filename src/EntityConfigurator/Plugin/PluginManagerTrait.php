<?php

namespace Drupal\records\EntityConfigurator\Plugin;

use Drupal\records\EntityConfigurator\Entity\EntityInterface;

/**
 * Trait that facilitates implementing entity configurator plugin managers.
 *
 * @see \Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface
 */
trait PluginManagerTrait {

  /**
   * Creates an instance based on the plugin info of the given entity.
   *
   * @param \Drupal\records\EntityConfigurator\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\records\EntityConfigurator\Plugin\PluginInterface
   *   The plugin.
   */
  public function createInstanceForEntity(
    EntityInterface $entity
  ) {
    return $this->createInstance(
      $entity->getPluginId(),
      $entity->getPluginConfiguration() ?? []
    );
  }

}
