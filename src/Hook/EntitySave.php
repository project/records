<?php

namespace Drupal\records\Hook;

use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\records\EntityConfigurator\Plugin\EntityPreSaveInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Holds methods implementing hooks related to entity saving.
 */
class EntitySave {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntitySave object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Implements `hook_entity_presave()`.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that is being saved.
   */
  public function preSave(EntityInterface $entity) {
    if (!$entity instanceof HasBundleEntityInterface) {
      return;
    }

    $entity_type = $this->entityTypeManager->getDefinition(
      $entity->getEntityTypeId()
    );

    $plugin = $this->entityTypeManager
      ->getStorage($entity_type->getBundleEntityType())
      ->loadWithPluginInstantiated($entity->bundle())
      ->getPlugin();

    if ($plugin instanceof EntityPreSaveInterface) {
      $plugin->entityPreSave($entity);
    }
  }

}
