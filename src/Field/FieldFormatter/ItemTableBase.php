<?php

namespace Drupal\records\Field\FieldFormatter;

use Drupal\records\Entity\EntityWithItemsInterface;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Base class for field formatters that render a table of items using a view.
 */
abstract class ItemTableBase extends FormatterBase {

  /**
   * Returns the ID of the view to use.
   *
   * @return string
   *   The ID of the view.
   */
  abstract protected function getViewId();

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();
    if (!$entity instanceof EntityWithItemsInterface) {
      throw new \InvalidArgumentException(
        'Can only use an item table widget on entities that have items.'
      );
    }

    $elements = [];

    $item_ids = $items->getEntity()->getItemIds();
    $elements[0] = [
      '#type' => 'view',
      '#name' => $this->getViewId(),
      '#arguments' => $item_ids ? [implode('+', $item_ids)] : NULL,
      '#embed' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * - We cannot check if the entity implements the `EntityWithItemsInterface`
   *   interface because that may not always be the case. Bundle entities will
   *   provide such implementation and that information is not available here
   *   because we don't have the instantiated entity. We therefore check if the
   *   field has the expected name instead, and we validate if the entity has
   *   items during the actual rendering of the field.
   * - We do not check for the field type; inheriting plugin implementations
   *   should declare the `entity_reference` type in their annotations.
   */
  public static function isApplicable(
    FieldDefinitionInterface $field_definition
  ) {
    if ($field_definition->getName() !== RecordField::ITEMS) {
      return FALSE;
    }

    return TRUE;
  }

}
