<?php

namespace Drupal\records_currency\Entity;

use Drupal\commerce_price\Entity\CurrencyInterface;
use Drupal\commerce_price\Price;
use Drupal\records\Entity\EntityWithAmountInterface;

/**
 * The interface for records that have currency enabled.
 *
 * On single-currency setups, the currency of the record amount will always be
 * the same as that of the account currency. Currency-enabled records are
 * records that their primary amount can be on a currency different than that of
 * the account currency.
 *
 * Currency-enabled records therefore have their own currency, an exchange rate,
 * and a normalized amount i.e. the amount converted to the account
 * currency. The normalized amount can then be used, for example, when
 * generating reports for the account.
 */
interface CurrencyEnabledEntityInterface extends EntityWithAmountInterface {

  /**
   * Returns the currency of the record.
   *
   * @return \Drupal\commerce_price\Entity\CurrencyInterface
   *   The currency.
   */
  public function getCurrency();

  /**
   * Sets the currency of the record.
   *
   * @param \Drupal\commerce_price\Entity\CurrencyInterface $currency
   *   The currency.
   *
   * @return $this
   */
  public function setCurrency(CurrencyInterface $currency);

  /**
   * Returns the currency exchange rate of the record.
   *
   * @return float
   *   The currency exchange rate.
   */
  public function getCurrencyExchangeRate();

  /**
   * Sets the currency exchange rate of the record.
   *
   * If a record is on the same currency as the account currency, the exchange
   * rate should not be set i.e. it should be NULL rather than setting it to
   * `1`.
   *
   * @param float|null $exchange_rate
   *   The currency exchange rate, or NULL to remove it.
   *
   * @return $this
   */
  public function setCurrencyExchangeRate(float $exchange_rate);

  /**
   * Returns the normalized amount of the record.
   *
   * @return \Drupal\commerce_price\Price
   *   The normalized amount.
   */
  public function getNormalizedAmount();

  /**
   * Sets the normalized amount of the record.
   *
   * @param \Drupal\commerce_price\Price $normalized_amount
   *   The normalized amount.
   */
  public function setNormalizedAmount(Price $normalized_amount);

}
