<?php

namespace Drupal\records_currency\Entity;

use Drupal\commerce_price\Entity\CurrencyInterface;
use Drupal\commerce_price\Price;
use Drupal\records\MachineName\Field\Record as RecordField;

/**
 * Trait that facilitates implementing currency-enabled records.
 *
 * @see \Drupal\records_currency\Entity\CurrencyEnabledEntityInterface
 */
trait CurrencyEnabledEntityTrait {

  /**
   * Returns the currency of the record.
   *
   * @return \Drupal\commerce_price\Entity\CurrencyInterface
   *   The currency.
   */
  public function getCurrency() {
    return $this->get(RecordField::CURRENCY)->entity;
  }

  /**
   * Sets the currency of the record.
   *
   * @param \Drupal\commerce_price\Entity\CurrencyInterface $currency
   *   The currency.
   *
   * @return $this
   */
  public function setCurrency(CurrencyInterface $currency) {
    $this->set(RecordField::CURRENCY, $currency);
    return $this;
  }

  /**
   * Returns the currency exchange rate of the record.
   *
   * @return float|null
   *   The currency exchange rate, or NULL if none is set.
   */
  public function getCurrencyExchangeRate() {
    $exchange_rate_field = $this->get(RecordField::CURRENCY_EXCHANGE_RATE);
    if ($exchange_rate_field->isEmpty()) {
      return;
    }

    return (float) $exchange_rate_field->value;
  }

  /**
   * Sets the currency exchange rate of the record.
   *
   * If a record is on the same currency as the account currency, the exchange
   * rate should not be set i.e. it should be NULL rather than setting it to
   * `1`.
   *
   * @param float|null $exchange_rate
   *   The currency exchange rate, or NULL to remove it.
   *
   * @return $this
   */
  public function setCurrencyExchangeRate($exchange_rate) {
    // phpcs:disable
    // @I Do we need to convert to `string` when storing into the field?
    //    type     : bug
    //    priority : normal
    //    labels   : currency, field
    // phpcs:enable
    $this->set(RecordField::CURRENCY_EXCHANGE_RATE, $exchange_rate);
    return $this;
  }

  /**
   * Returns the normalized amount of the record.
   *
   * @return \Drupal\commerce_price\Price
   *   The normalized amount.
   */
  public function getNormalizedAmount() {
    return $this->get(RecordField::NORMALIZED_AMOUNT)->first()->toPrice();
  }

  /**
   * Sets the normalized amount of the record.
   *
   * @param \Drupal\commerce_price\Price $normalized_amount
   *   The normalized amount.
   */
  public function setNormalizedAmount(Price $normalized_amount) {
    $this->set(RecordField::NORMALIZED_AMOUNT, $normalized_amount);
  }

}
