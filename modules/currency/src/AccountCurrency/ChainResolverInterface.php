<?php

namespace Drupal\records_currency\AccountCurrency;

/**
 * The interface for the account currency resolution chain.
 *
 * We always need to define primary and available currencies on accounts in
 * order to limit and/or pre-select the currency for amounts on records that
 * belong to the account. They may also be used to facilitate multi-currency
 * setups, and for generating reports. Depending on the application logic, they
 * may be coming from a user field, or a group field etc.
 *
 * Providing chain resolution gives the opportunity to custom or contrib modules
 * to determine the account currency based on their requirements.
 */
interface ChainResolverInterface extends
  ResolverInterface {

  /**
   * Adds a resolver.
   *
   * @param \Drupal\records_currency\AccountCurrency\ResolverInterface $resolver
   *   The resolver.
   */
  public function addResolver(ResolverInterface $resolver);

  /**
   * Returns all resolvers.
   *
   * @return \Drupal\records_currency\AccountCurrency\ResolverInterface[]
   *   The resolvers.
   */
  public function getResolvers();

}
