<?php

namespace Drupal\records_currency\AccountCurrency;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * The interface for account currency resolvers.
 *
 * @see \Drupal\records_currency\AccountCurrency\ChainResolverInterface
 */
interface ResolverInterface {

  /**
   * Returns the account currency for the given entity.
   *
   * The entity may be a User or a Record.
   *
   * Commonly, the account currency for a record would be the account currency
   * for the user that the record belongs to - even though it depends on the
   * application logic.
   *
   * @param \Drupal\Core\User\UserInterface|\Drupal\records_currency\Entity\RecordInterface $entity
   *   The entity.
   *
   * @return \Drupal\commerce_price\Entity\CurrencyInterface
   *   The currency entity.
   */
  public function resolveCurrency(ContentEntityInterface $entity);

  /**
   * Returns the available account currencies for the given entity.
   *
   * The entity may be a User or a Record.
   *
   * Commonly, the available account currencies for a record would be the
   * available account currencies for the user that the record belongs to - even
   * though it depends on the application logic.
   *
   * @param \Drupal\Core\User\UserInterface|\Drupal\records_currency\Entity\RecordInterface $entity
   *   The entity.
   *
   * @return \Drupal\commerce_price\Entity\CurrencyInterface[]
   *   The currency entity.
   */
  public function resolveAvailableCurrencies(ContentEntityInterface $entity);

}
