<?php

namespace Drupal\records_currency\AccountCurrency;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Default implementation of the chain account currency resolver.
 */
class ChainResolver implements
  ChainResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Drupal\records_currency\AccountCurrency\ResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Constructs a new ChainResolver object.
   *
   * @param \Drupal\records_currency\AccountCurrency\ResolverInterface[] $resolvers
   *   The resolvers.
   */
  public function __construct(array $resolvers = []) {
    $this->resolvers = $resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function addResolver(ResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolvers() {
    return $this->resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveCurrency(ContentEntityInterface $entity = NULL) {
    foreach ($this->resolvers as $resolver) {
      $result = $resolver->resolveCurrency($entity);
      if ($result) {
        return $result;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resolveAvailableCurrencies(
    ContentEntityInterface $entity = NULL
  ) {
    foreach ($this->resolvers as $resolver) {
      $result = $resolver->resolveAvailableCurrencies($entity);
      if ($result) {
        return $result;
      }
    }
  }

}
