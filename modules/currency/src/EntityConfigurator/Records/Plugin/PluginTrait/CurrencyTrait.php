<?php

namespace Drupal\records_currency\EntityConfigurator\Records\Plugin\PluginTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\records\Entity\EntityWithItemsInterface;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\records_currency\Entity\CurrencyEnabledEntityInterface;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Facilitates implementing entities that have a currency.
 *
 * Records do not need to have a currency on single-currency setups as there's
 * only one currency on the account. For cases, however, where different records
 * on the account may be issued in different currencies, we need to be
 * converting them to the primary currency of the account.
 *
 * We therefore store on the record the currency and the exchange rate for that
 * conversion, as well as the result i.e. the normalized amount. That normalized
 * amount should then be used, for example, for combining records in different
 * currencies when generating reports.
 */
trait CurrencyTrait {

  /**
   * The account currency resolver.
   *
   * @var \Drupal\records_currency\AccountCurrency\ResolverInterface
   */
  protected $accountCurrencyResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * Returns the field definitions for records with currency.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildCurrencyFieldDefinitions() {
    $fields = [];

    $fields[RecordField::CURRENCY] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Currency'))
      ->setDescription(new TranslatableMarkup('The currency of the entity.'))
      ->setCardinality(1)
      ->setSetting('target_type', 'commerce_currency')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields[RecordField::CURRENCY_EXCHANGE_RATE] = BundleFieldDefinition::create('float')
      ->setLabel(new TranslatableMarkup('Currency exchange rate'))
      ->setDescription(new TranslatableMarkup(
        'The exchange rate that will be used for currency conversions related to
         the entity.'
      ))
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Returns the field definitions for records with a normalized amount.
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   The field definitions.
   */
  protected function buildNormalizedAmountFieldDefinitions() {
    $fields = [];

    $fields[RecordField::NORMALIZED_AMOUNT] = BundleFieldDefinition::create('commerce_price')
      ->setLabel(new TranslatableMarkup('Normalized amount'))
      ->setDescription(new TranslatableMarkup(
        'The amount of the entity converted to the currency of the account.'
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Generates and sets the normalized amount from the record primary amount.
   *
   * @param \Drupal\records_currency\Entity\CurrencyEnabledEntityInterface $entity
   *   The record entity.
   */
  protected function setNormalizedAmount(
    CurrencyEnabledEntityInterface $entity
  ) {
    [
      $normalize,
      $account_currency,
      $exchange_rate,
    ] = $this->amountNormalizationInfo($entity);

    $amount = $entity->getAmount();

    // If we're not normalizing, we simply copy the amount. We still want to
    // have the normalized amount field populated so that it is easier to
    // compile reports using one field i.e. all values coming from the
    // normalized amount field rather than having to collect values from one
    // field for some records and from another for others.
    if (!$normalize) {
      $entity->setNormalizedAmount($amount);
      return;
    }

    if ($exchange_rate === NULL) {
      throw new \RuntimeException(
        'Cannot normalize the amounts for a record that does not have an exchange rate.'
      );
    }

    // phpcs:disable
    // @I Round the normalized amount
    //    type     : bug
    //    priority : normal
    //    labels   : currency
    // phpcs:enable
    $normalized_amount = $amount->convert(
      $account_currency,
      // We convert to string because that's expected by the price calculator
      // that does the convesion.
      (string) $exchange_rate
    );
    $entity->setNormalizedAmount($this->rounder->round($normalized_amount));
  }

  /**
   * Generates and sets the normalized amounts for all record items.
   *
   * The normalized amount for each item is generated from the same item's
   * primary amount. We do not generate the normalized amounts when the items
   * are being saved because we may not have the parent entity available in the
   * pre-save hook. That is because the default implementation assumes managing
   * items via `inline_entity_form` and we may not store a back-reference to the
   * parent entity, and even if a custom implementation does have store
   * back-reference on the item, the parent entity could still not be available
   * for new entities since the items would normally be saved first so that
   * their IDs are then stored on the `items` field on the parent entity.
   *
   * We therefore do not have the currency and the exchange rate available on
   * item pre-save.
   *
   * @param \Drupal\records_currency\Entity\CurrencyEnabledEntityInterface $entity
   *   The parent record entity.
   */
  protected function setNormalizedAmountForItems(
    CurrencyEnabledEntityInterface $entity
  ) {
    if (!$entity instanceof EntityWithItemsInterface) {
      throw new \InvalidArgumentException(
        'Cannot normalize the item amounts for a record that does not have items.'
      );
    }

    [
      $normalize,
      $account_currency,
      $exchange_rate,
    ] = $this->amountNormalizationInfo($entity);

    // All items must be of the same entity type. Let's be a bit more efficient
    // and don't request the storage for every item.
    $storage = NULL;
    foreach ($entity->getItems() as $item) {
      if (!$item instanceof CurrencyEnabledEntityInterface) {
        throw new \InvalidArgumentException(
          'Cannot normalize the amount for a record that is not currency-enabled.'
        );
      }

      if ($storage === NULL) {
        $storage = $this->entityTypeManager->getStorage(
          $item->getEntityTypeId()
        );
      }
      $amount = $item->getAmount();

      // If we're not normalizing, we simply copy the amount. We still want to
      // have the normalized amount field populated so that it is easier to
      // compile reports using one field i.e. all values coming from the
      // normalized amount field rather than having to collect values from one
      // field for some records and from another for others.
      if (!$normalize) {
        $item->setNormalizedAmount($amount);
        $storage->save($item);
        continue;
      }

      if ($exchange_rate === NULL) {
        throw new \RuntimeException(
          'Cannot normalize the item amounts for a record that does not have an exchange rate.'
        );
      }

      // phpcs:disable
      // @I Round the normalized amount
      //    type     : bug
      //    priority : normal
      //    labels   : currency
      // phpcs:enable
      $normalized_amount = $amount->convert(
        $account_currency,
        // We convert to string because that's expected by the price calculator
        // that does the convesion.
        (string) $exchange_rate
      );
      $item->setNormalizedAmount($this->rounder->round($normalized_amount));
      $storage->save($item);
    }
  }

  /**
   * Returns information pertaining to the normalization of a record's amounts.
   *
   * @param \Drupal\records_currency\Entity\CurrencyEnabledEntityInterface $entity
   *   The parent record entity.
   *
   * @return array
   *   An array containing the following keys/values:
   *   - 0 (boolean): Whether amounts should be normalized for the given record
   *     and its items.
   *   - 1 (string): The ID of the account currency e.g. `USD`.
   *   - 2 (string|null): The exchange rate, or NULL if none was set on the
   *     record.
   */
  protected function amountNormalizationInfo(
    CurrencyEnabledEntityInterface $entity
  ) {
    $record_currency = $entity->getCurrency()->id();
    $account_currency = $this->accountCurrencyResolver
      ->resolveCurrency($entity)
      ->id();

    // We will be normalizing the amount only if the record is on a currency
    // different than the account currency.
    if ($account_currency === $record_currency) {
      return [
        FALSE,
        $account_currency,
        NULL,
      ];
    }

    return [
      TRUE,
      $account_currency,
      $entity->getCurrencyExchangeRate(),
    ];
  }

}
