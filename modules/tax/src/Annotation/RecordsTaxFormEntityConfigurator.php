<?php

namespace Drupal\records_tax\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the tax form configurator plugins.
 *
 * Plugin namespace: Plugin\Records\EntityConfigurator\TaxForm.
 *
 * @Annotation
 */
class RecordsTaxFormEntityConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The workflow ID for the tax form state field.
   *
   * @var string
   */
  public $workflow_id;

}
