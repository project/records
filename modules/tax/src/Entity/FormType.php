<?php

namespace Drupal\records_tax\Entity;

use Drupal\records\Entity\BundleConfigEntityBase;

/**
 * The default implementation of the Tax Form Type configuration entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "records_tax_form_type",
 *   label = @Translation("Tax form type"),
 *   label_singular = @Translation("tax form type"),
 *   label_plural = @Translation("tax form types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tax form type",
 *     plural = "@count tax form types"
 *   ),
 *   admin_permission = "administer records_tax_form_type",
 *   config_prefix = "tax_form_type",
 *   bundle_of = "records_tax_form",
 *   bundle_label = @Translation("Tax form type configurator"),
 *   bundle_plugin_type = "records_tax_form_configurator",
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\records_tax\Entity\Controller\FormTypeListBuilder",
 *     "storage" = "Drupal\records_tax\Entity\Storage\FormType",
 *     "form" = {
 *       "add" = "Drupal\records_tax\Entity\Form\FormType",
 *       "edit" = "Drupal\records_tax\Entity\Form\FormType",
 *       "delete" = "Drupal\records_tax\Entity\Form\FormTypeDelete",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/records/config/tax-forms/types/add",
 *     "add-form" = "/admin/records/config/tax-forms/types/add/{plugin_id}",
 *     "edit-form" = "/admin/records/config/tax-forms/types/{records_tax_form_type}/edit",
 *     "delete-form" = "/admin/records/config/tax-forms/types/{records_tax_form_type}/delete",
 *     "collection" = "/admin/records/config/tax-forms/types",
 *   },
 * )
 * phpcs:enable
 */
class FormType extends BundleConfigEntityBase implements
  FormTypeInterface {}
