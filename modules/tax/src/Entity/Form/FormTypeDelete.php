<?php

namespace Drupal\records_tax\Entity\Form;

use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The default form controller for Tax Form Type deletion forms.
 */
class FormTypeDelete extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $count = $this->entityTypeManager
      ->getStorage('records_tax_form')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition(RecordField::TYPE, $this->entity->id())
      ->count()
      ->execute();
    if (!$count) {
      return parent::buildForm($form, $form_state);
    }

    $form['#title'] = $this->getQuestion();
    $form['description'] = [
      '#markup' => '<p>' . $this->formatPlural(
        $count,
        '%type is used by 1 tax form. You cannot remove this type until you have
         removed all of the %type tax forms.',
        '%type is used by @count tax forms. You cannot remove %type until you
         have removed all of the %type tax forms.',
        ['%type' => $this->entity->label()]
      ) . '</p>',
    ];
    return $form;
  }

}
