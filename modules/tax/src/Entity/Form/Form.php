<?php

namespace Drupal\records_tax\Entity\Form;

use Drupal\records\Entity\Form\ContentEntityBase;

/**
 * The default form controller for Tax Form forms.
 */
class Form extends ContentEntityBase {}
