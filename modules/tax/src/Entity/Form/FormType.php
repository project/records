<?php

namespace Drupal\records_tax\Entity\Form;

use Drupal\records\Entity\Form\BundleConfigEntityBase;
use Drupal\records\EntityConfigurator\Entity\Form\ConfigFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default form controller for Tax Form Type forms.
 */
class FormType extends BundleConfigEntityBase {

  use ConfigFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.records_tax'),
      $container->get('plugin.manager.records_tax_form_configurator'),
      $container->get('string_translation')
    );
  }

}
