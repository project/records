<?php

namespace Drupal\records_tax\Entity;

use Drupal\records\Entity\RecordInterface;
use Drupal\records\Entity\StatefulEntityInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\user\EntityOwnerInterface;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * The interface for Tax Form content entities.
 */
interface FormInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityOwnerInterface,
  HasBundleEntityInterface,
  RecordInterface,
  StatefulEntityInterface {}
