<?php

namespace Drupal\records_tax\Entity;

use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * The interface for Tax Form Type configuration entities.
 */
interface FormTypeInterface extends
  ConfigEntityInterface,
  ConfigurableEntityInterface,
  EntityDescriptionInterface {}
