<?php

namespace Drupal\records_tax\EntityConfigurator\Form;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginInterface as ConfiguratorPluginInterface;

/**
 * Provides the interface for tax form configurator plugins.
 *
 * Tax form configurator plugins are responsible for configuring the behavior
 * of tax form types. That includes the following:
 *
 * - Defines the workflow followed by the tax form.
 * - Generates the tax form title, if needed.
 * - Calculates any computed fields, if needed.
 * - Provides any fields required by the plugin.
 */
interface PluginInterface extends ConfiguratorPluginInterface {}
