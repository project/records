<?php

namespace Drupal\records_tax\EntityConfigurator\Form;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginBase as ConfiguratorPluginBase;
use Drupal\records\Exception\InvalidConfigurationException;

/**
 * Default base class for tax form configurator plugin implementations.
 */
abstract class PluginBase extends ConfiguratorPluginBase implements
  PluginInterface {

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (!isset($plugin_definition['workflow_id'])) {
      throw new InvalidConfigurationException(sprintf(
        'No workflow defined by plugin with ID "%s".',
        $plugin_id
      ));
    }
  }

}
