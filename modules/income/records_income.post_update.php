<?php

/**
 * @file
 * Post update functions for the Records Income module.
 */

/**
 * Imports the income item table view if it doesn't already exist.
 */
function records_income_post_update_1() {
  \Drupal::service('records_income.updater.post_1')->run();
}
