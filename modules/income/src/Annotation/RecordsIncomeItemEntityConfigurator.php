<?php

namespace Drupal\records_income\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the income item configurator plugins.
 *
 * Plugin namespace: Plugin\Records\EntityConfigurator\IncomeItem.
 *
 * @Annotation
 */
class RecordsIncomeItemEntityConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
