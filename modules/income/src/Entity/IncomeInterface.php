<?php

namespace Drupal\records_income\Entity;

use Drupal\records\Entity\EntityWithAmountInterface;
use Drupal\records\Entity\RecordInterface;
use Drupal\records\Entity\StatefulEntityInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * The interface for Income content entities.
 */
interface IncomeInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityOwnerInterface,
  EntityWithAmountInterface,
  HasBundleEntityInterface,
  RecordInterface,
  StatefulEntityInterface {}
