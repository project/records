<?php

namespace Drupal\records_income\Entity;

use Drupal\records\Entity\BundleConfigEntityBase;

/**
 * The default implementation of the Income Item Type configuration entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "records_income_item_type",
 *   label = @Translation("Income item type"),
 *   label_singular = @Translation("income item type"),
 *   label_plural = @Translation("income item types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count income item type",
 *     plural = "@count income item types"
 *   ),
 *   admin_permission = "administer records_income_type",
 *   config_prefix = "income_item_type",
 *   bundle_of = "records_income_item",
 *   bundle_label = @Translation("Income item type configurator"),
 *   bundle_plugin_type = "records_income_item_configurator",
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\records_income\Entity\Controller\IncomeItemTypeListBuilder",
 *     "storage" = "Drupal\records_income\Entity\Storage\IncomeItemType",
 *     "form" = {
 *       "add" = "Drupal\records_income\Entity\Form\IncomeItemType",
 *       "edit" = "Drupal\records_income\Entity\Form\IncomeItemType",
 *       "delete" = "Drupal\records_income\Entity\Form\IncomeItemTypeDelete",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/records/config/income/item-types/add",
 *     "add-form" = "/admin/records/config/income/item-types/add/{plugin_id}",
 *     "edit-form" = "/admin/records/config/income/item-types/{records_income_item_type}/edit",
 *     "delete-form" = "/admin/records/config/income/item-types/{records_income_item_type}/delete",
 *     "collection" = "/admin/records/config/income/item-types",
 *   },
 * )
 * phpcs:enable
 */
class IncomeItemType extends BundleConfigEntityBase implements
  IncomeItemTypeInterface {}
