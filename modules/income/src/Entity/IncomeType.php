<?php

namespace Drupal\records_income\Entity;

use Drupal\records\Entity\BundleConfigEntityBase;

/**
 * The default implementation of the Income Type configuration entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "records_income_type",
 *   label = @Translation("Income type"),
 *   label_singular = @Translation("income type"),
 *   label_plural = @Translation("income types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count income type",
 *     plural = "@count income types"
 *   ),
 *   admin_permission = "administer records_income_type",
 *   config_prefix = "income_type",
 *   bundle_of = "records_income",
 *   bundle_label = @Translation("Income type configurator"),
 *   bundle_plugin_type = "records_income_configurator",
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\records_income\Entity\Controller\IncomeTypeListBuilder",
 *     "storage" = "Drupal\records_income\Entity\Storage\IncomeType",
 *     "form" = {
 *       "add" = "Drupal\records_income\Entity\Form\IncomeType",
 *       "edit" = "Drupal\records_income\Entity\Form\IncomeType",
 *       "delete" = "Drupal\records_income\Entity\Form\IncomeTypeDelete",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/records/config/income/types/add",
 *     "add-form" = "/admin/records/config/income/types/add/{plugin_id}",
 *     "edit-form" = "/admin/records/config/income/types/{records_income_type}/edit",
 *     "delete-form" = "/admin/records/config/income/types/{records_income_type}/delete",
 *     "collection" = "/admin/records/config/income/types",
 *   },
 * )
 * phpcs:enable
 */
class IncomeType extends BundleConfigEntityBase implements
  IncomeTypeInterface {}
