<?php

namespace Drupal\records_income\Entity;

use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * The interface for Income Type configuration entities.
 */
interface IncomeTypeInterface extends
  ConfigEntityInterface,
  ConfigurableEntityInterface,
  EntityDescriptionInterface {}
