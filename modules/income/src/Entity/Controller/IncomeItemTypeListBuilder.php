<?php

namespace Drupal\records_income\Entity\Controller;

use Drupal\records\Entity\Controller\BundleConfigEntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default list builder for Income Item Types.
 */
class IncomeItemTypeListBuilder extends BundleConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.records_income_item_configurator')
    );
  }

}
