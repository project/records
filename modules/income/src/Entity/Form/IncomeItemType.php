<?php

namespace Drupal\records_income\Entity\Form;

use Drupal\records\Entity\Form\BundleConfigEntityBase;
use Drupal\records\EntityConfigurator\Entity\Form\ConfigFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default form controller for Income Item Type forms.
 */
class IncomeItemType extends BundleConfigEntityBase {

  use ConfigFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.records_income'),
      $container->get('plugin.manager.records_income_item_configurator'),
      $container->get('string_translation')
    );
  }

}
