<?php

namespace Drupal\records_income\Entity\Form;

use Drupal\records\Entity\Form\ContentEntityBase;

/**
 * The default form controller for Income forms.
 */
class Income extends ContentEntityBase {}
