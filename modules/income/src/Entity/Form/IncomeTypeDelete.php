<?php

namespace Drupal\records_income\Entity\Form;

use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The default form controller for Income Type deletion forms.
 */
class IncomeTypeDelete extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $count = $this->entityTypeManager
      ->getStorage('records_income')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition(RecordField::TYPE, $this->entity->id())
      ->count()
      ->execute();
    if (!$count) {
      return parent::buildForm($form, $form_state);
    }

    $form['#title'] = $this->getQuestion();
    $form['description'] = [
      '#markup' => '<p>' . $this->formatPlural(
        $count,
        '%type is used by 1 income record. You cannot remove this type until
         you have removed all of the %type income records.',
        '%type is used by @count income records. You cannot remove %type until
         you have removed all of the %type income records.',
        ['%type' => $this->entity->label()]
      ) . '</p>',
    ];
    return $form;
  }

}
