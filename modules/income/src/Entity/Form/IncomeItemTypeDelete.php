<?php

namespace Drupal\records_income\Entity\Form;

use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The default form controller for Income Item Type deletion forms.
 */
class IncomeItemTypeDelete extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $count = $this->entityTypeManager
      ->getStorage('records_income_item')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition(RecordField::TYPE, $this->entity->id())
      ->count()
      ->execute();
    if (!$count) {
      return parent::buildForm($form, $form_state);
    }

    $form['#title'] = $this->getQuestion();
    $form['description'] = [
      '#markup' => '<p>' . $this->formatPlural(
        $count,
        '%type is used by 1 income item. You cannot remove this type until
         you have removed all of the %type income items.',
        '%type is used by @count income items. You cannot remove %type until
         you have removed all of the %type income items.',
        ['%type' => $this->entity->label()]
      ) . '</p>',
    ];
    return $form;
  }

}
