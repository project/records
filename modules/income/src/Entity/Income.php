<?php

namespace Drupal\records_income\Entity;

use Drupal\records\Entity\EntityWithAmountTrait;
use Drupal\records\Entity\StatefulContentEntityTrait;
use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\user\EntityOwnerTrait;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The default implementation of the Income content entity.
 *
 * phpcs:disable
 * @ContentEntityType(
 *   id = "records_income",
 *   label = @Translation("Income"),
 *   label_collection = @Translation("Income"),
 *   label_singular = @Translation("income record"),
 *   label_plural = @Translation("income records"),
 *   label_count = @PluralTranslation(
 *     singular = "@count income record",
 *     plural = "@count income records"
 *   ),
 *   bundle_label = @Translation("Income type", context = "Records"),
 *   bundle_entity_type = "records_income_type",
 *   field_ui_base_route = "entity.records_income_type.edit_form",
 *   handlers = {
 *     "list_builder" = "Drupal\records_income\Entity\Controller\IncomeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\records_income\Entity\Form\Income",
 *       "add" = "Drupal\records_income\Entity\Form\Income",
 *       "edit" = "Drupal\records_income\Entity\Form\Income",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\records_income\Entity\Routing\IncomeRouteProvider",
 *     }
 *   },
 *   base_table = "records_income",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "owner_id",
 *     "label" = "title",
 *     "owner" = "owner_id",
 *   },
 *   admin_permission = "administer records_income",
 *   permission_granularity = "bundle",
 *   links = {
 *     "canonical" = "/income/{records_income}",
 *     "add-page" = "/income/add",
 *     "add-form" = "/income/add/{records_income_type}",
 *     "edit-form" = "/income/{records_income}/edit",
 *     "delete-form" = "/income/{records_income}/delete",
 *     "collection" = "/admin/records/income",
 *   }
 * )
 *
 * @I Make the Income entity revisionable
 *    type     : feature
 *    priority : high
 *    labels   : income
 * @I Add reference to the contact entity
 *    type     : feature
 *    priority : high
 *    labels   : income
 * @I Add plugin interface for price fields (total_price/total_paid/balance)
 *    type     : feature
 *    priority : high
 *    labels   : income
 * @I Add interface methods related to base fields e.g. `getAmount`
 *    type     : feature
 *    priority : normal
 *    labels   : income
 * phpcs:enable
 */
class Income extends ContentEntityBase implements IncomeInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityWithAmountTrait;
  use StatefulContentEntityTrait;

  /**
   * {@inheritdoc}
   *
   * We make the entity intentionally lightweight to allow for simple income
   * records as well. Additional fields will be added via plugins and field UI.
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // We expose the type to the UI, view only.
    $fields[RecordField::TYPE]
      ->setLabel(new TranslatableMarkup('Type'))
      ->setDisplayConfigurable('view', TRUE);

    // Title/subject for the record. It may be programmatically generated by the
    // configurator plugin associated with the type of the record.
    $fields[RecordField::TITLE] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setDescription(new TranslatableMarkup(
        'A short title explaining what the income record is about.'
      ))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Amount of the record. It may be programmatically generated by the
    // configurator plugin associated with the type of the record.
    $fields[RecordField::AMOUNT] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(new TranslatableMarkup('Amount'))
      ->setDescription(new TranslatableMarkup(
        'The amount of the income record.'
      ))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields[RecordField::STATE] = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setDescription(new TranslatableMarkup(
        'The state of the income record.'
      ))
      ->setRequired(TRUE)
      ->setSetting(
        'workflow_callback',
        [__CLASS__, 'getWorkflowId']
      )
      ->setDisplayConfigurable('view', TRUE);

    // Add the Owner field.
    // The owner is essentially the assignee i.e. the user currently responsible
    // for the income record. Information about who created and changed the
    // record would be kept once we make the entity revisionable.
    // phpcs:disable
    // @I Provide widget for only selecting users with appropriate roles
    // phpcs:enable
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields[RecordField::OWNER_ID]
      ->setLabel(new TranslatableMarkup('Owner'))
      ->setDescription(new TranslatableMarkup(
        'The user currently responsible for the income record.'
      ))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions(
        'view',
        [
          'label' => 'hidden',
          'type' => 'author',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'entity_reference_autocomplete',
          'weight' => 5,
          'settings' => [
            'placeholder' => 'Assign the income record to a user',
          ],
        ]
      );

    // Timestamps.
    $fields[RecordField::CREATED] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time that the income record was created.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields[RecordField::CHANGED] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time that the income record was last edited.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Returns the ID of the workflow for the state field.
   *
   * @param \Drupal\records_income\Entity\IncomeInterface $income
   *   The income record.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(IncomeInterface $income) {
    return \Drupal::service('entity_type.manager')
      ->getStorage('records_income_type')
      ->loadWithPluginInstantiated($income->bundle())
      ->getPlugin()
      ->getPluginDefinition()['workflow_id'];
  }

}
