<?php

namespace Drupal\records_income\Entity;

use Drupal\records\Entity\EntityWithAmountInterface;
use Drupal\records\Entity\EntityItemInterface;
use Drupal\records\Entity\RecordInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * The interface for Income Item content entities.
 */
interface IncomeItemInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityItemInterface,
  EntityWithAmountInterface,
  HasBundleEntityInterface,
  RecordInterface {}
