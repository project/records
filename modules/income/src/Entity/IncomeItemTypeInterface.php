<?php

namespace Drupal\records_income\Entity;

use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * The interface for Income Item Type configuration entities.
 */
interface IncomeItemTypeInterface extends
  ConfigEntityInterface,
  ConfigurableEntityInterface,
  EntityDescriptionInterface {}
