<?php

namespace Drupal\records_income\Plugin\Field\FieldFormatter;

use Drupal\records\Field\FieldFormatter\ItemTableBase;

/**
 * Renders the income item table using the default view.
 *
 * phpcs:disable
 * @FieldFormatter(
 *   id = "records_income_item_table",
 *   label = @Translation("Income item table"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 * phpcs:enable
 */
class IncomeItemTable extends ItemTableBase {

  /**
   * {@inheritdoc}
   */
  protected function getViewId() {
    return 'records_income_item_table';
  }

}
