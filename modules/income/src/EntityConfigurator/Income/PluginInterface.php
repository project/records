<?php

namespace Drupal\records_income\EntityConfigurator\Income;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginInterface as ConfiguratorPluginInterface;

/**
 * Provides the interface for income configurator plugins.
 *
 * Income configurator plugins are responsible for configuring the behavior
 * of income types. That includes the following:
 *
 * - Generates the income record title, if needed.
 * - Calculates the income record total and balance.
 * - Provides any fields required by the plugin.
 */
interface PluginInterface extends ConfiguratorPluginInterface {}
