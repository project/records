<?php

namespace Drupal\records_income\EntityConfigurator\IncomeItem;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginInterface as ConfiguratorPluginInterface;

/**
 * Provides the interface for income item configurator plugins.
 *
 * Income item configurator plugins are responsible for configuring the behavior
 * of income item types. That includes the following:
 *
 * - Generates the income item title, if needed.
 * - Calculates the income item amount.
 * - Provides any fields required by the plugin.
 */
interface PluginInterface extends ConfiguratorPluginInterface {}
