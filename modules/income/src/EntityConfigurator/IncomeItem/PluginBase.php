<?php

namespace Drupal\records_income\EntityConfigurator\IncomeItem;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginBase as ConfiguratorPluginBase;

/**
 * Default base class for income item configurator plugin implementations.
 */
abstract class PluginBase extends ConfiguratorPluginBase implements
  PluginInterface {

}
