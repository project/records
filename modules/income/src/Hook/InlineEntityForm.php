<?php

namespace Drupal\records_income\Hook;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Holds methods implementing hooks related to inline entity forms.
 */
class InlineEntityForm {

  use StringTranslationTrait;

  /**
   * Constructs a new InlineEntityForm object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    // Injections required by traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * Alter the fields used to represent an entity in the IEF table.
   *
   * @param array $fields
   *   The fields, keyed by field name.
   * @param array $context
   *   An array with the following keys:
   *   - parent_entity_type: The type of the parent entity.
   *   - parent_bundle: The bundle of the parent entity.
   *   - field_name: The name of the reference field on which IEF is operating.
   *   - entity_type: The type of the referenced entities.
   *   - allowed_bundles: Bundles allowed on the reference field.
   *
   * @see \Drupal\inline_entity_form\InlineFormInterface::getTableFields()
   */
  public function alterIncomeItemTableFields(array &$fields, array $context) {
    if ($context['entity_type'] !== 'records_income_item') {
      return;
    }

    // Most commonly it's not required to display the type. It is obvious from
    // its title.
    unset($fields['type']);

    $fields['amount'] = [
      'type' => 'field',
      'label' => $this->t('Amount'),
      'weight' => 1,
    ];
  }

}
