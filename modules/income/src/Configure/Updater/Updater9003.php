<?php

namespace Drupal\records_income\Configure\Updater;

use Drupal\records\MachineName\Field\Record as RecordField;
use Drupal\records_income\Entity\Income;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Runs the 9003 updates.
 *
 * Adds `amount` to the `records_income` base fields.
 */
class Updater9003 {

  /**
   * The Records Income entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdaterManagerInterface
   */
  protected $updateManager;

  /**
   * Constructs a new Updater9003 object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDefinitionUpdateManagerInterface $update_manager
  ) {
    $this->entityType = $entity_type_manager->getDefinition('records_income');
    $this->updateManager = $update_manager;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    $this->updateManager->installFieldStorageDefinition(
      RecordField::AMOUNT,
      'records_income',
      'records_income',
      Income::baseFieldDefinitions($this->entityType)[RecordField::AMOUNT]
    );
  }

}
