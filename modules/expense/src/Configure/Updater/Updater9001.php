<?php

namespace Drupal\records_expense\Configure\Updater;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Runs the 9001 updates.
 *
 * Installs the `expense_item` entity type.
 */
class Updater9001 {

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdaterManagerInterface
   */
  protected $updateManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Updater9004 object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDefinitionUpdateManagerInterface $update_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    $entity_type_ids = [
      'records_expense_item',
      'records_expense_item_type',
    ];

    foreach ($entity_type_ids as $entity_type_id) {
      // Do not install if already installed.
      $entity_type = $this->updateManager->getEntityType($entity_type_id);
      if ($entity_type) {
        return;
      }

      $this->updateManager->installEntityType(
        $this->entityTypeManager->getDefinition($entity_type_id)
      );
    }
  }

}
