<?php

namespace Drupal\records_expense\Configure\Updater;

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Runs the 9001 updates.
 *
 * Installs the `records_expense_item_table` view.
 */
class PostUpdater1 {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PostUpdater1 object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    $view_id = 'records_expense_item_table';
    $storage = $this->entityTypeManager->getStorage('view');

    // Nothing to do if the view already exists.
    if ($storage->load($view_id)) {
      return;
    }

    $source = new FileStorage(
      drupal_get_path('module', 'records_expense') . '/config/optional'
    );
    $storage->create($source->read("views.view.{$view_id}"))->save();
  }

}
