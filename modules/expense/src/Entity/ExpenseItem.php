<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\Entity\EntityWithAmountTrait;
use Drupal\records\MachineName\Field\Record as RecordField;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The default implementation of the Expense Item content entity.
 *
 * phpcs:disable
 * @ContentEntityType(
 *   id = "records_expense_item",
 *   label = @Translation("Expense item"),
 *   label_collection = @Translation("Expense item"),
 *   label_singular = @Translation("expense item"),
 *   label_plural = @Translation("expense items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count expense item",
 *     plural = "@count expense items"
 *   ),
 *   bundle_label = @Translation("Expense item type", context = "Records"),
 *   bundle_entity_type = "records_expense_item_type",
 *   field_ui_base_route = "entity.records_expense_item_type.edit_form",
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *   },
 *   base_table = "records_expense_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *   },
 *   admin_permission = "administer records_expense",
 *   permission_granularity = "bundle",
 * )
 *
 * @I Make the Expense Item entity revisionable
 *    type     : feature
 *    priority : high
 *    labels   : expense
 * @I Add interface methods related to base fields e.g. `getAmount`
 *    type     : feature
 *    priority : normal
 *    labels   : expense
 * phpcs:enable
 */
class ExpenseItem extends ContentEntityBase implements ExpenseItemInterface {

  use EntityChangedTrait;
  use EntityWithAmountTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // We expose the type to the UI, view only.
    $fields[RecordField::TYPE]
      ->setLabel(new TranslatableMarkup('Type'))
      ->setDisplayConfigurable('view', TRUE);

    // Title/subject for the item.
    $fields[RecordField::TITLE] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setDescription(new TranslatableMarkup(
        'A short title explaining what the expense item is about.'
      ))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Amount of the item.
    $fields[RecordField::AMOUNT] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(new TranslatableMarkup('Amount'))
      ->setDescription(new TranslatableMarkup(
        'The amount of the expense item.'
      ))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Timestamps.
    $fields[RecordField::CREATED] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time that the expense item was created.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields[RecordField::CHANGED] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time that the expense item was last edited.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
