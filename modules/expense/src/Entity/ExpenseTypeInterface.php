<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\EntityConfigurator\Entity\EntityInterface as ConfigurableEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * The interface for Expense Type configuration entities.
 */
interface ExpenseTypeInterface extends
  ConfigEntityInterface,
  ConfigurableEntityInterface,
  EntityDescriptionInterface {}
