<?php

namespace Drupal\records_expense\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default list builder for expense records.
 *
 * phpcs:disable
 * @I Replace admin list builder with view
 *    type     : improvement
 *    priority : normal
 *    labels   : expense
 * @I Display the expense state
 *    type     : improvement
 *    priority : low
 *    labels   : ux
 * @I Display date information about the expense
 *    type     : improvement
 *    priority : low
 *    labels   : ux
 * phpcs:enable
 */
class ExpenseListBuilder extends EntityListBuilder {

  /**
   * The expense type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $typeStorage;

  /**
   * The bundle labels.
   *
   * We may have multiple expense records of the same type listed. To avoid
   * loading the expense type entity every time - even if it's from the cache,
   * we cache here the expense type labels.
   *
   * @var array
   */
  protected $typeLabels = [];

  /**
   * Constructs a new ExpenseListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityStorageInterface $type_storage
   *   The expense type storage.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityStorageInterface $type_storage
  ) {
    parent::__construct($entity_type, $storage);

    $this->typeStorage = $type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $entity_type_manager->getStorage($entity_type->getBundleEntityType())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['title'] = $this->t('Title');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $type_id = $entity->bundle();
    if (!isset($this->typeLabels[$type_id])) {
      $this->typeLabels[$type_id] = $this->typeStorage->load($type_id)->label();
    }

    $row['id'] = $entity->id();
    $row['type'] = $this->typeLabels[$type_id];
    $row['title'] = $entity->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('view') && $entity->hasLinkTemplate('canonical')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }

    return $operations;
  }

}
