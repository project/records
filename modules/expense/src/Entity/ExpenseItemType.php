<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\Entity\BundleConfigEntityBase;

/**
 * The default implementation of the Expense Item Type configuration entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "records_expense_item_type",
 *   label = @Translation("Expense item type"),
 *   label_singular = @Translation("expense item type"),
 *   label_plural = @Translation("expense item types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count expense item type",
 *     plural = "@count expense item types"
 *   ),
 *   admin_permission = "administer records_expense_type",
 *   config_prefix = "expense_item_type",
 *   bundle_of = "records_expense_item",
 *   bundle_label = @Translation("Expense item type configurator"),
 *   bundle_plugin_type = "records_expense_item_configurator",
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\records_expense\Entity\Controller\ExpenseItemTypeListBuilder",
 *     "storage" = "Drupal\records_expense\Entity\Storage\ExpenseItemType",
 *     "form" = {
 *       "add" = "Drupal\records_expense\Entity\Form\ExpenseItemType",
 *       "edit" = "Drupal\records_expense\Entity\Form\ExpenseItemType",
 *       "delete" = "Drupal\records_expense\Entity\Form\ExpenseItemTypeDelete",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/records/config/expense/item-types/add",
 *     "add-form" = "/admin/records/config/expense/item-types/add/{plugin_id}",
 *     "edit-form" = "/admin/records/config/expense/item-types/{records_expense_item_type}/edit",
 *     "delete-form" = "/admin/records/config/expense/item-types/{records_expense_item_type}/delete",
 *     "collection" = "/admin/records/config/expense/item-types",
 *   },
 * )
 * phpcs:enable
 */
class ExpenseItemType extends BundleConfigEntityBase implements
  ExpenseItemTypeInterface {}
