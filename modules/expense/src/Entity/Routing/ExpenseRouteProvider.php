<?php

namespace Drupal\records_expense\Entity\Routing;

use Drupal\records\Entity\Controller\Entity as EntityController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider as RouteProviderBase;

use Symfony\Component\Routing\Route;

/**
 * Provides routes for the Expense entity.
 *
 * We override the parent to provide an `add_page` route that uses our custom
 * entity controller `addPage()` method.
 */
class ExpenseRouteProvider extends RouteProviderBase {

  /**
   * {@inheritdoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();
    $route = new Route($entity_type->getLinkTemplate('add-page'));

    $route->setDefault('_controller', EntityController::class . '::addPage');
    $route->setDefault('_title_callback', EntityController::class . '::addTitle');
    $route->setDefault('entity_type_id', $entity_type_id);
    $route->setRequirement('_entity_create_any_access', $entity_type_id);

    return $route;
  }

}
