<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\Entity\BundleConfigEntityBase;

/**
 * The default implementation of the Expense Type configuration entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "records_expense_type",
 *   label = @Translation("Expense type"),
 *   label_singular = @Translation("expense type"),
 *   label_plural = @Translation("expense types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count expense type",
 *     plural = "@count expense types"
 *   ),
 *   admin_permission = "administer records_expense_type",
 *   config_prefix = "expense_type",
 *   bundle_of = "records_expense",
 *   bundle_label = @Translation("Expense type configurator"),
 *   bundle_plugin_type = "records_expense_configurator",
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\records_expense\Entity\Controller\ExpenseTypeListBuilder",
 *     "storage" = "Drupal\records_expense\Entity\Storage\ExpenseType",
 *     "form" = {
 *       "add" = "Drupal\records_expense\Entity\Form\ExpenseType",
 *       "edit" = "Drupal\records_expense\Entity\Form\ExpenseType",
 *       "delete" = "Drupal\records_expense\Entity\Form\ExpenseTypeDelete",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/records/config/expense/types/add",
 *     "add-form" = "/admin/records/config/expense/types/add/{plugin_id}",
 *     "edit-form" = "/admin/records/config/expense/types/{records_expense_type}/edit",
 *     "delete-form" = "/admin/records/config/expense/types/{records_expense_type}/delete",
 *     "collection" = "/admin/records/config/expense/types",
 *   },
 * )
 * phpcs:enable
 */
class ExpenseType extends BundleConfigEntityBase implements
  ExpenseTypeInterface {}
