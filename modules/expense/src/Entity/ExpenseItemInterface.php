<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\Entity\EntityWithAmountInterface;
use Drupal\records\Entity\EntityItemInterface;
use Drupal\records\Entity\RecordInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * The interface for Expense Item content entities.
 */
interface ExpenseItemInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityItemInterface,
  EntityWithAmountInterface,
  HasBundleEntityInterface,
  RecordInterface {}
