<?php

namespace Drupal\records_expense\Entity\Storage;

use Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface;
use Drupal\records\EntityConfigurator\Entity\Storage\StorageInterface;
use Drupal\records\EntityConfigurator\Entity\Storage\StorageTrait;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default expense item type storage class.
 */
class ExpenseItemType extends ConfigEntityStorage implements
  StorageInterface {

  use StorageTrait;

  /**
   * Constructs an ExpenseItemType object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend.
   * @param \Drupal\records\EntityConfigurator\Plugin\PluginManagerInterface $plugin_manager
   *   The configurator plugin manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    ConfigFactoryInterface $config_factory,
    UuidInterface $uuid_service,
    LanguageManagerInterface $language_manager,
    MemoryCacheInterface $memory_cache,
    PluginManagerInterface $plugin_manager
  ) {
    parent::__construct(
      $entity_type,
      $config_factory,
      $uuid_service,
      $language_manager,
      $memory_cache
    );

    // Injections required by traits.
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    return new static(
      $entity_type,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('plugin.manager.records_expense_item_configurator')
    );
  }

}
