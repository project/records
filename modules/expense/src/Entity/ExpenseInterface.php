<?php

namespace Drupal\records_expense\Entity;

use Drupal\records\Entity\EntityWithAmountInterface;
use Drupal\records\Entity\RecordInterface;
use Drupal\records\Entity\StatefulEntityInterface;
use Drupal\records\EntityConfigurator\Entity\HasBundleEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * The interface for Expense content entities.
 */
interface ExpenseInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityOwnerInterface,
  EntityWithAmountInterface,
  HasBundleEntityInterface,
  RecordInterface,
  StatefulEntityInterface {}
