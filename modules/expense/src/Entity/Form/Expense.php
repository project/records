<?php

namespace Drupal\records_expense\Entity\Form;

use Drupal\records\Entity\Form\ContentEntityBase;

/**
 * The default form controller for Expense forms.
 */
class Expense extends ContentEntityBase {}
