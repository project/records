<?php

namespace Drupal\records_expense\Entity\Form;

use Drupal\records\Entity\Form\BundleConfigEntityBase;
use Drupal\records\EntityConfigurator\Entity\Form\ConfigFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default form controller for Expense Item Type forms.
 */
class ExpenseItemType extends BundleConfigEntityBase {

  use ConfigFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.records_expense'),
      $container->get('plugin.manager.records_expense_item_configurator'),
      $container->get('string_translation')
    );
  }

}
