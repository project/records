<?php

namespace Drupal\records_expense\EntityConfigurator\Expense;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginInterface as ConfiguratorPluginInterface;

/**
 * Provides the interface for expense configurator plugins.
 *
 * Expense configurator plugins are responsible for configuring the behavior
 * of expense types. That includes the following:
 *
 * - Generates the expense record title, if needed.
 * - Calculates the expense record total and balance.
 * - Provides any fields required by the plugin.
 */
interface PluginInterface extends ConfiguratorPluginInterface {}
