<?php

namespace Drupal\records_expense\EntityConfigurator\ExpenseItem;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginInterface as ConfiguratorPluginInterface;

/**
 * Provides the interface for expense item configurator plugins.
 *
 * Expense item configurator plugins are responsible for configuring the
 * behavior of expense item types. That includes the following:
 *
 * - Generates the expense item title, if needed.
 * - Calculates the expense item amount.
 * - Provides any fields required by the plugin.
 */
interface PluginInterface extends ConfiguratorPluginInterface {}
