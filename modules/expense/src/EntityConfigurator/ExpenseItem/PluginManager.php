<?php

namespace Drupal\records_expense\EntityConfigurator\ExpenseItem;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginManagerBase;
use Drupal\records_expense\Annotation\RecordsExpenseItemEntityConfigurator as PluginAnnotation;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * The default plugin manager for expense item configurator plugins.
 */
class PluginManager extends PluginManagerBase {

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/Records/EntityConfigurator/ExpenseItem',
      $namespaces,
      $module_handler,
      PluginInterface::class,
      PluginAnnotation::class
    );

    $this->alterInfo('records_expense_item_configurator_info');
    $this->setCacheBackend(
      $cache_backend,
      'records_expense_item_configurator',
      ['records_expense_item_configurator_plugins']
    );
  }

}
