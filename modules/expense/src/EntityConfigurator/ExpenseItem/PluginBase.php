<?php

namespace Drupal\records_expense\EntityConfigurator\ExpenseItem;

use Drupal\records\EntityConfigurator\Records\Plugin\PluginBase as ConfiguratorPluginBase;

/**
 * Default base class for expense item configurator plugin implementations.
 */
abstract class PluginBase extends ConfiguratorPluginBase implements
  PluginInterface {}
