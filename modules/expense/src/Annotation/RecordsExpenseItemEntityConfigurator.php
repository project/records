<?php

namespace Drupal\records_expense\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the expense item configurator plugins.
 *
 * Plugin namespace: Plugin\Records\EntityConfigurator\ExpenseItem.
 *
 * @Annotation
 */
class RecordsExpenseItemEntityConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
