<?php

namespace Drupal\records_expense\Hook;

use Drupal\views\ViewExecutable;

/**
 * Holds methods implementing hooks related to views.
 */
class Views {

  /**
   * Sorts the views results as referenced in the items field.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   */
  public function sortItemTable(ViewExecutable $view) {
    if ($view->id() !== 'records_expense_item_table') {
      return;
    }
    if (!$view->result) {
      return;
    }

    // Sort items as passed in the view arguments, which are sorted by their
    // deltas in the `items` field.
    $unsorted_results = $view->result;
    $sorted_ids = explode('+', $view->argument['id']->argument);
    // We filter the results because the arguments may contain IDs that do not
    // show up in the actual results. That could happen due to access check.
    $view->result = array_filter(array_map(
      function ($id) use ($unsorted_results) {
        foreach ($unsorted_results as $result) {
          if ($result->id === $id) {
            return $result;
          }
        }
      },
      $sorted_ids
    ));
  }

}
