<?php

namespace Drupal\records_expense\Plugin\Field\FieldFormatter;

use Drupal\records\Field\FieldFormatter\ItemTableBase;

/**
 * Renders the expense item table using the default view.
 *
 * phpcs:disable
 * @FieldFormatter(
 *   id = "records_expense_item_table",
 *   label = @Translation("Expense item table"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 * phpcs:enable
 */
class ExpenseItemTable extends ItemTableBase {

  /**
   * {@inheritdoc}
   */
  protected function getViewId() {
    return 'records_expense_item_table';
  }

}
