<?php

/**
 * @file
 * Post update functions for the Records Expense module.
 */

/**
 * Imports the expense item table view if it doesn't already exist.
 */
function records_expense_post_update_1() {
  \Drupal::service('records_expense.updater.post_1')->run();
}
